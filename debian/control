Source: ucblogo
Section: devel
Priority: optional
Maintainer: Barak A. Pearlmutter <bap@debian.org>
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://people.eecs.berkeley.edu/~bh/logo.html
Build-Depends: debhelper-compat (= 13),
	autoconf-archive,
	libncurses-dev, libx11-dev, libsm-dev, libice-dev, libwxgtk3.2-dev,
	texlive-latex-base, texinfo, ghostscript
Vcs-Git: https://salsa.debian.org/debian/ucblogo.git
Vcs-Browser: https://salsa.debian.org/debian/ucblogo

Package: ucblogo
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: dialect of lisp using turtle graphics famous for teaching kids
 This is the UC Berkeley implementation of logo written primarily by
 Daniel Van Blerkom, Brian Harvey, Michael Katz, and Douglas Orleans.  This
 version of logo is featured in Brian Harvey's book
 _Computer_Science_Logo_Style, _Volume_1: _Symbolic_Computing_
 (ISBN 0-262-58151-5).  This version provides the following special features:
 .
  - Random-access arrays.
  - Variable number of inputs to user-defined procedures.
  - Mutators for list structure (dangerous).
  - Pause on error, and other improvements to error handling.
  - Comments and continuation lines; formatting is preserved when
    procedure definitions are saved or edited.
  - Terrapin-style tokenization (e.g., [2+3] is a list with one member)
    but LCSI-style syntax (no special forms except TO).  The best of
    both worlds.
  - First-class instruction and expression templates.
  - Macros.
